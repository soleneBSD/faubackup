faubackup (0.5.9) unstable; urgency=low

  * support for custom config file locations (Closes: #273140)
  * really propagate error status of child processes (Closes: #348316)
  * check for ..inodes directories (Closes: #357865)
  * Make error messages more consistant and clear (Closes: #329866)
  * Support for O_NOATIME

 -- Martin Waitz <tali@debian.org>  Wed, 26 Apr 2006 23:57:06 +0200

faubackup (0.5.8) unstable; urgency=low

  * fix umask, thanks for the NMU (Closes: #243255)
  * fix documentation about find usage (Closes: #250097,#250098)
  * support for --one-file-system
  * debian/control: add perl-modules dependency

 -- Martin Waitz <tali@debian.org>  Thu, 15 Apr 2004 19:43:35 +0200

faubackup (0.5.7.1) unstable; urgency=low

  * debian/control: add libpopt-dev to build depends

 -- Martin Waitz <tali@debian.org>  Sat, 10 Apr 2004 19:18:16 +0200

faubackup (0.5.7) unstable; urgency=low

  * by default, atime is not preserved any more
  * now uses stdio instead of direct syscalls
  * should fully work on solaris now (Closes: #148065)

 -- Martin Waitz <tali@debian.org>  Mon, 29 Mar 2004 00:35:37 +0200

faubackup (0.5.6) unstable; urgency=low

  * Switch build system to automake
  * Use CDBS for packaging

 -- Martin Waitz <tali@debian.org>  Thu, 11 Dec 2003 01:48:36 +0100

faubackup (0.5.5) unstable; urgency=low

  * really fix Makefile and version... *arg*

 -- Martin Waitz <tali@debian.org>  Thu, 16 Oct 2003 11:09:52 +0200

faubackup (0.5.4) unstable; urgency=low

  * don't warn if faubackup-gather can't reset accesstime

 -- Martin Waitz <tali@debian.org>  Wed, 15 Oct 2003 23:41:14 +0200

faubackup (0.5.3) unstable; urgency=low

  * Support for incremental backups by non-root users.
  * Fix build process
  * debian/rules: explicitly use gcc, cleanup

 -- Martin Waitz <tali@debian.org>  Wed, 15 Oct 2003 22:21:21 +0200

faubackup (0.5.2) unstable; urgency=low

  * call check_ignore on converted ignore line (Closes: #187014, #200398)
  * possibility to disable full regexps
  * document end-of-backup marker (Closes: #198443)
  * better faubackup-find dokumentation (hopefully Closes: #200401)
  * debian/control: Standards-Version 3.6.1.0
    (no changes to package neccessary)
  * debian/rules: correctly use time stamps

 -- Martin Waitz <tali@debian.org>  Mon,  1 Sep 2003 23:53:33 +0200

faubackup (0.5.1) unstable; urgency=low

  * introduce $getroot config variable (Closes: #156921)
  * Abort faubackup-scatter on ENOSPC (Closes: #149157)
  * Do a syntax check on regular expressions
  * Document that PATH must be correctly set on remote machines
    (Closes: #143255)
  * debian/control: Standards-Version 3.5.9.0
    (no changes to package neccessary)

 -- Martin Waitz <tali@debian.org>  Wed, 13 Mar 2003 23:53:23 +0100

faubackup (0.5.0) unstable; urgency=low

  * Make it a debian-native package
    it's easier for me to maintain it this way
  * include large file support (thanks to david.jericho@bytecomm.com.au)
  * Use "." as end marker to detect broken backups (Closes: #145822)
  * support autocreate for remote destdirs (thanks to alnikolov@narod.ru)
    (Closes: #156908)
  * Correct hostname parsing (Closes: #177323)
  * remove show version -V switch (Closes: #158183,#169396)
  * don't use '=' in build directory name
  * debian/control: Standards-Version 3.5.8.0
    (no changes to package neccessary)

 -- Martin Waitz <tali@debian.org>  Wed,  5 Mar 2003 23:53:23 +0100

faubackup (0.5pre1-2) unstable; urgency=high

  * yes, it's late, but perhaps it'll make it into woody ;)
  * merged upstream bugfix and documentation patches...
    - handle interupted system calls
    - update documentation to reflect changed --find option
      Closes: #144385
    - better error output
  * debian/control: Standards-Version 3.5.6.1
    (no changes to package neccessary)

 -- Martin Waitz <tali@debian.org>  Mon, 25 Mar 2002 23:56:30 +0100

faubackup (0.5pre1-1) unstable; urgency=low

  * New upsteam release
    - name change from vbackup to faubackup

 -- Martin Waitz <tali@debian.org>  Wed,  7 Mar 2002 17:29:52 +0200

vbackup (0.2.3-1) unstable; urgency=low

  * New upstream release
  - new --list option to display info about backups
  - now -depth is not used any more for find
  - find parameter may be specified in configfile
  - option to keep last daily backup instead of first

 -- Martin Waitz <tali@debian.org>  Wed, 30 May 2001 00:29:52 +0200

vbackup (0.2.2-1) unstable; urgency=low

  * upstream bugfix release
  - remote host handling should work now (Closes: #94624)
  - atime of files is preserved on backup (Closes: #96296)
  - documentation fixes (Closes: #93095)
  * Maintainer email set to tali@debian.org.
  * new Standards version (3.5.4), no changes neccessary

 -- Martin Waitz <tali@debian.org>  Mon, 21 May 2001 15:27:59 +0200

vbackup (0.2.1-1) unstable; urgency=medium

  * upstream bugfix release
  - fixed broken reread-on-shortread (Closes: #91784).
  - added NAME section to vbackup.conf.5

 -- Martin Waitz <tali@stud.uni-erlangen.de>  Wed,  4 Apr 2001 02:13:22 +0200

vbackup (0.2-1) unstable; urgency=low

  * New upstream release
  - supports filenames of arbitrary length
  - directories now get correct timestamps
  - does not rely on st_size==size of link (fixes #85436)
  - split program into two binaries
  - Updated Documentation (fixes #85927)
  - fixed config file handling
  - included copyright notice in source files
  - removed cvs logs from source files
  * bumped Standards-Version to 3.5.2 (no changes neccessary)
  * now using debhelper 3
  * changed eMail Address

 -- Martin Waitz <tali@stud.uni-erlangen.de>  Wed, 14 Mar 2001 00:23:17 +0100

vbackup (0.1-3) unstable; urgency=low

  * Added vbackup-clean to remove obsolete backups.
  * Speed-Up in comparision of files
  * Makefile changes
  * small changes to cron.daily example
  * removed README.Debian
  * changed description

 -- Martin Waitz <tali@rommelwood.de>  Mon, 22 Jan 2001 10:29:04 +0100

vbackup (0.1-2) unstable; urgency=low

  * Rebuild using dpkg-source 1.8.3

 -- Martin Waitz <tali@rommelwood.de>  Sat, 13 Jan 2001 21:25:43 +0100

vbackup (0.1-1) unstable; urgency=low

  * Initial Release.

 -- Martin Waitz <tali@rommelwood.de>  Tue, 19 Dec 2000 14:47:20 +0100
