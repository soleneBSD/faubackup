# Signature of the current package.
m4_define([AT_PACKAGE_NAME],      [FauBackup])
m4_define([AT_PACKAGE_TARNAME],   [faubackup])
m4_define([AT_PACKAGE_VERSION],   [0.5.9])
m4_define([AT_PACKAGE_STRING],    [FauBackup 0.5.9])
m4_define([AT_PACKAGE_BUGREPORT], [faubackup-user@lists.sourceforge.net])
