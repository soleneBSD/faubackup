/*
 *  FAUBackup - Backup System, using a Filesystem for Storage
 *  Copyright (c) 2000-01 Dr. Volkmar Sieh, 2000-06 Martin Waitz
 *  $Id$
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#define B_IFIFO		0010000
#define B_IFCHR		0020000
#define B_IFDIR		0040000
#define B_IFBLK		0060000
#define B_IFREG		0100000
#define B_IFLNK		0120000
#define B_IFSOCK	0140000

#define B_IFSIZE64	0200000

#define MAXLEN		1024

